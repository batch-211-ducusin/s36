// Create the schema, model, and export the file.

const mongoose = require('mongoose');
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pendin"
	}
});

module.exports = mongoose.model("Task", taskSchema);