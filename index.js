/*
Modules and Prameterized Routes


Separation of Concerns
Why do we have to?
	- Better code readability
	- Improved scalability
	- Better code maintainability
What do we separate?
	* Models
		- contains WHAT object are needed in our API
		- Object schemas and relationships
	* Controllers
		- contains instruction on HOW your api will perform its intended tasks
		- Mongoose model queries are used here, examples are:
			Model.find()
	* Routes
		- Defines WHEN particular controllers will be used.
*/


// setup the dependencies or imports
const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes');

// server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
// Connecting to MongoDB Atlas
mongoose.connect('mongodb+srv://admin123:admin123@cluster0.cxdyx16.mongodb.net/s36?retryWrites=true&w=majority',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
let db = mongoose.connection
db.on('error', ()=> console.error('Connection Error.'));
db.once('open', ()=> console.log('Connected to MongoDB!'));

app.use('/tasks', taskRoutes);

app.listen(port, ()=> console.log(`Server running at port ${port}`));