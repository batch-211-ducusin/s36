// Controllers contain the functions and business logic of our express JS Application
// Meaning all the operations it can do will be placed in this file.

const Task = require('../models/Task');

// Controller function for getting all the tasks

module.exports.getAllTasks = ()=> {
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody)=> {
	let newTask = new Task({
		name : requestBody.name
	})
	return newTask.save().then((task, error)=> {
		if(error){
			console.log(error);
			return false;
		} else {
			return task;
		}
	})
}

module.exports.deleteTask = (taskId)=> {
	return Task.findByIdAndRemove(taskId).then((removedTask, err)=> {
		if(err) {
			console.log(err);
			return false;
		} else {
			return removedTask;
		}
	})
}


// Ito ay mag dedelete at the same time mag aadd o mag uupdate ng new object
module.exports.updateTask = (taskId, newContent)=> {
	return Task.findById(taskId).then((result, error)=> {
		if(error){
			console.log(err);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask, saveErr)=> {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}




// s36 Activity
module.exports.getTasks = (taskId)=> {
	return Task.findOne({taskId}).then(result => {
		return result;
	})
}

module.exports.updateTask = (taskId, newStatus)=> {
	return Task.findById(taskId).then((result, error)=> {
		if(error){
			console.log(err);
			return false;
		}
		result.status = newStatus.status;
		return result.save().then((updatedTask, saveErr)=> {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return updatedTask;
			}
		})
	})
}